#!/bin/bash

USER=$(whoami)
if test $USER == 'root'
then
echo "This is running from root user"
elif test $USER == 'jenkins'
then
echo "You are running using jenkins user"
fi